setwd("D:/����� ������ ������/��� �/����� �/����� ��� ������ �����/������ ���/EX7")
contact<-read.csv("contact.csv")
myStopwords <- readLines("stopwords.csv")
library(caTools)
library(ggplot2)
library(gridExtra)
library(caret)
library(mice)
library(outliers)
library(e1071)
library(moderndive)
library(effects)
library(outliers)
library(dplyr)
library(tm)
library(wordcloud)
library(e1071)
library(SnowballC)

str(contact)
summary(contact)
head(contact)


# ����� ������� ������
contact$category<-as.factor(contact$category)


ggplot(contact, aes(category)) + geom_bar()
# ����� ���� 700 �������� �� ������ ��� ����� �400 �� �����

# ����� ��������
contact_corpus <- Corpus(VectorSource(contact$text))
contact_corpus[[1]][[1]]

# ����� ������
clean_corpus <- tm_map(contact_corpus, removePunctuation)
clean_corpus[[1]][[1]]

# ����� ������ ��������
clean_corpus <- tm_map(clean_corpus, removeNumbers)
clean_corpus[[1]][[1]]

# ���� stopwords
clean_corpus <- tm_map(clean_corpus, removeWords, myStopwords)
clean_corpus[[1]][[1]]


# ���� ������
clean_corpus <- tm_map(clean_corpus, stripWhitespace)
clean_corpus[[1]][[1]]

# 
dtm <- DocumentTermMatrix(clean_corpus)
dim(dtm)

#remove infrequent attributes (words)
frequent_dtm <- DocumentTermMatrix(clean_corpus, list(dictionary = findFreqTerms(dtm,10))) 
dim(frequent_dtm)




#color pallet 
pal <- brewer.pal(9,'Dark2')

wordcloud(clean_corpus[contact$category == 'sales'], min.freq = 5, random.order = TRUE, colors = pal)
wordcloud(clean_corpus[contact$category == 'support'], min.freq = 5, random.order = TRUE, colors = pal)





#remove infrequent attributes (words)
frequent_dtm <- DocumentTermMatrix(clean_corpus, list(dictionary = findFreqTerms(dtm,10))) 
dim(frequent_dtm)


#replace numbers with 'yes' or 'no' 

convert_yesno <- function(x){
  if(x==0) return ('no')
  return ('yes')
}


yesno_matrix <- apply(frequent_dtm, MARGIN = 1:2, convert_yesno)

#convert to datafame 
contact.df <- as.data.frame(yesno_matrix)

str(contact.df)

#add the labels (target) column 
contact.df$type <- contact$category

str(contact.df)

dim(contact.df)

randomvector <- runif(500)
filter <- randomvector > 0.5

contact.df.train <- contact.df[filter,]
contact.df.test <- contact.df[!filter,]

dim(contact.df.train)
dim(contact.df.test)

#run naive base algorithm to generate a model 

model <- naiveBayes(contact.df.train[,-60],contact.df.train$type)

#prediction on the test set 
prediction <- predict(model,contact.df.test[,-60], type = 'raw')

prediction_contact <- prediction[,'support']
actual <- contact.df.test$type
predicted <- prediction_contact > 0.6

#confusion matrix 
conf_matrix <- table(predicted,actual)

#compute precision and recall 
precision <- conf_matrix['TRUE','support']/(conf_matrix['TRUE','support']+conf_matrix['TRUE','sales'])
recall <- conf_matrix['TRUE','support']/(conf_matrix['TRUE','support']+conf_matrix['FALSE','support'])



