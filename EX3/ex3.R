##���� 1

titanic.new<-read.csv('train.csv',header = TRUE,na.strings = "")
titanic.function<-function(x)
{
  vecFalseOrTrue<-apply(X = x, MARGIN = 2,function(x) any(is.na(x)))
  return (vecFalseOrTrue)
}
titanic.function(titanic.new)



##���� 2
replaceNAtoMedian<-function(x)
{
  y <- sapply(x,is.numeric)
  x[y] <- lapply(x[y], function(y) replace(y, is.na(y), median(y, na.rm = TRUE)))
  return(x[y])
}

titanic.no.NA<-replaceNAtoMedian(titanic.new)
titanic.no.NA


##���� 3
titanic2<-titanic.new
integer_vec<-sapply(titanic.new,is.integer)
integer_vec<-as.factor(titanic.new)
class(integer_vec)

titanic2[,'PassengerId'] <- factor(titanic2[,'PassengerId'], levels= 1, labels = ("A"))
class(titanic2$PassengerId)

titanic2[,'Pclass'] <- factor(titanic2[,'Pclass'], levels= 2, labels = ("B"))
class(titanic2$Pclass)

titanic2[,'SibSp'] <- factor(titanic2[,'SibSp'], levels= 3, labels = ("C"))
class(titanic2$SibSp)

titanic2[,'Parch'] <- factor(titanic2[,'Parch'], levels= 4, labels = ("D"))
class(titanic2$Parch)
