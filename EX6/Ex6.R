setwd("D:/����� ������ ������/��� �/����� �/����� ��� ������ �����/������ ���/EX6")
library(caTools)
library(ggplot2)
library(gridExtra)
library(caret)
library(mice)
library(outliers)
library(e1071)
library(moderndive)
library(effects)
library(outliers)
library(dplyr)

weather<-read.csv('weatherAUS.csv')


## ����� ����� ����, ���� ����
weather$year<-as.factor(substring(weather$Date,1,4))
weather$month<-as.factor(substring(weather$Date,6,7))
weather$day<-as.factor(substring(weather$Date,9,10))

str(weather)
summary(weather)



## �����
ggplot(data = weather,aes(as.factor(day))) + geom_bar() 

ggplot(data = weather,aes(as.factor(month))) + geom_bar()

ggplot(data = weather,aes(as.factor(year))) + geom_bar()

ggplot(data = weather,aes(as.factor(weather$year))) +geom_bar(aes(fill = weather$RainToday),position='fill')
  




## ����� �������
## ����� NA 
## ������ �� ���� �����
getmode <- function(v) {
  uniqv <- unique(v)
  uniqv[which.max(tabulate(match(v, uniqv)))]
}#Function for removing outliers
out.rem<-function(x) {
  x[which(x==outlier(x))]=NA
  x
}
weather=weather %>% mutate_if(is.numeric, funs(replace(.,is.na(.), mean(., na.rm = TRUE)))) %>%
  mutate_if(is.factor, funs(replace(.,is.na(.), getmode(na.omit(.)))))
summary(weather)




weather$Date<-NULL
weather$Location<-NULL
weather$MinTemp<-NULL
weather$MaxTemp<-NULL
weather$Rainfall<-NULL
weather$Evaporation<-NULL
weather$Sunshine<-NULL


## ���� 1
filter<-sample.split(weather$RainTomorrow, SplitRatio=0.7)
rain.train<-subset(weather, filter==TRUE)
rain.test<-subset(weather, filter==FALSE)

dim(rain.train)
dim(rain.test)

# ���� �����
rain.model <- glm(weather$RainToday~.,family = binomial(link = 'logit'), data = rain.train)
summary(married.model)


## ����� ����� �����
predicted.loan.test<-predict(married.model, newdata = married.test,type = 'response' )
## response = �������
## ������ ���� �� ����� ���� �������

## ������� �� �� ����� ������� ����� �0.5
## ���� �� ����� �� ������
confiusion.matrix<-table(predicted.loan.test >0.46,married.test$Loan_Status)
## ���� ����� ����� ����� ���� �0.46 �����

# ���� �� ���� ���� ��:
# TP, FP, FN, TN
precision<-(confiusion.matrix[2,2])/((confiusion.matrix[2,2]+confiusion.matrix[2,1]))
recall<-(confiusion.matrix[2,2])/(confiusion.matrix[2,2]+confiusion.matrix[1,2])
