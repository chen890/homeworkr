setwd("D:/����� ������ ������/��� �/����� �/����� ��� ������ �����/������ ���/EX5")
house.data<-read.csv("kc_house_data.csv",na.strings = "")
library(caTools)
library(ggplot2)
library(gridExtra)

# ����� ���� ��� �� ����� �������
house.copy<-house.data
str(house.copy)
summary(house.copy)

# ����� ������ ���� ����, ��� �����
bought.year<- substring(text = house.copy$date,first =1 ,last = 4)
bought.month<-substring(text = house.copy$date, first =5 ,last = 6)
bought.day<-substring(text = house.copy$date, first =7 , last = 8)
house.copy$bought.year<-bought.year
house.copy$bought.month<-bought.month
house.copy$bought.day<-bought.day
house.copy$date<-NULL
house.copy$zipcode<-NULL
house.copy$long<-NULL
house.copy$yr_renovated<-NULL
as.factor(house.copy$bought.year)
as.factor(house.copy$bought.month)
as.factor(house.copy$bought.day)

# ����� ��� ���� ��� ���� ���� ������ �� �����
ggplot(data = house.copy, aes(as.factor(bought.year), price)) + geom_boxplot()

# ����� ��� ���� ������ ������ �� �����
ggplot(data = house.copy, aes(as.factor(house.copy$floors), price)) +geom_point()
ggplot(data = house.copy, aes(as.factor(house.copy$floors), price)) +geom_boxplot()

# ��� ���� ������ ������ �� �����
ggplot(data = house.copy, aes(as.factor(house.copy$bedrooms), price))+geom_boxplot()

# ��� ���� ����� ���� ������ �� �����
ggplot(data = house.copy, aes(as.factor(house.copy$yr_built), price))+geom_col()

# ��� ���� ���� ������� ������� �� �����
ggplot(data = house.copy, aes(as.factor(house.copy$bathrooms), price))+geom_col()
ggplot(data = house.copy, aes(as.factor(house.copy$bathrooms), price))+geom_point()

# ��� ����� ����� �� ����� �� ����
ggplot(data = house.copy, aes(as.factor(house.copy$grade), price))+geom_col()

# ��� ��� ���� ����� �� �����
ggplot(data = house.copy, aes(as.factor(house.copy$condition), price))+geom_col()

# ����� ������� ���������
summary(house.copy$price)
price.breaks<-c(0,75001,250000,400000,800000,1000000,3000000,7000000,20000000)
price.labels<- c("up to 75K","75k-250K","250k-400k","400k-800k","800k-1m","1m-3m","3m-7m","7m-20m")

# ��� ����� ��� ��� ������� ����� ���� ���� ���� ����
ggplot(data = house.copy, aes(bins, house.copy$bedrooms)) +geom_boxplot() 
#���� ����� ��� ����� ���� ��� ��� 30 ���� ���� ����� ����� ��� 400 �800 ���

# bin the data
#cut ����� �� ������� ��������
bins<-cut(x = house.copy$price,breaks = price.breaks,labels = price.labels,include.lowest = TRUE,right = FALSE)
summary(bins)
house.copy$Levels<-bins


## ����� ���� 1
filter.price<- sample.split(Y = house.copy$price,SplitRatio = 0.75)
filter.price
house.train.price <- subset(x = house.copy, filter.price==TRUE)
house.test.price <- subset(x = house.copy, filter.price==FALSE)

# ���� �� ��������
dim(house.train.price)
dim(house.test.price)

## ���� �������� ����� ����
#�������� lm
#linear model
model.price <- lm(price~. ,house.train.price)
summary(model.price)

##������� ���� ����� �� ��� ������ ��� ����� �� ����� �� ��
#��� ��� ���� ������ - ���� �����
predict.train.price <-predict(object = model.price, newdata = house.train.price)
predict.test.price <-predict(object = model.price, newdata = house.test.price)

MSE.train.price <- mean(((x =house.copy$price)-predict.train.price)**2)
MSE.test.price <- mean(((x =house.copy$price)-predict.test.price)**2)
MSE.train.price
MSE.test.price
sqrt(MSE.train.price)
sqrt(MSE.test.price)
mean(house.copy$price)
## ���� ����� �� ����� ����� ��� ����� ���� ���� ������� ������ �� ����� �����

## ����� ���� 2
filter.sqft_living <- sample.split(Y = house.copy$sqft_living,SplitRatio = 0.75)
filter.sqft_living
house.train.sqft_living<- subset(x = house.copy, filter.sqft_living==TRUE)
house.test.sqft_living <- subset(x = house.copy, filter.sqft_living==FALSE)

# ���� �� ��������
dim(house.train.sqft_living)
dim(house.test.sqft_living)

## ���� �������� ����� ����
#�������� lm
#linear model
model.sqft_living <- lm(sqft_living~. ,house.train.sqft_living)
summary(model.sqft_living)

##������� ���� ����� �� ��� ������ ��� ����� �� ����� �� ��
#��� ��� ���� ������ - ���� �����
predict.train.sqft_living <-predict(object = model.sqft_living, newdata = house.train.sqft_living)
predict.test.sqft_living <-predict(object = model.sqft_living, newdata = house.test.sqft_living)

MSE.train.sqft_living <- mean(((x =house.copy$sqft_living)-predict.train.sqft_living)**2)
MSE.test.sqft_living <- mean(((x =house.copy$sqft_living)-predict.test.sqft_living)**2)
MSE.train.sqft_living
MSE.test.sqft_living
sqrt(MSE.train.sqft_living)
sqrt(MSE.test.sqft_living)
mean(house.copy$sqft_living)
## ���� ����� �� ����� ����� ��� ����� ���� ���� ������� ������ �� ����� �����


## ����� ���� 3
filter.sqft_lot <- sample.split(Y = house.copy$sqft_lot,SplitRatio = 0.75)
filter.sqft_lot
house.train.sqft_lot<- subset(x = house.copy, filter.sqft_lot==TRUE)
house.test.sqft_lot <- subset(x = house.copy, filter.sqft_lot==FALSE)

# ���� �� ��������
dim(house.train.sqft_lot)
dim(house.test.sqft_lot)

## ���� �������� ����� ����
#�������� lm
#linear model
model.sqft_lot <- lm(sqft_lot~. ,house.train.sqft_lot)
summary(model.sqft_lot)

##������� ���� ����� �� ��� ������ ��� ����� �� ����� �� ��
#��� ��� ���� ������ - ���� �����
predict.train.sqft_lot <-predict(object = model.sqft_lot, newdata = house.train.sqft_lot)
predict.test.sqft_lot <-predict(object = model.sqft_lot, newdata = house.test.sqft_lot)

MSE.train.sqft_lot <- mean(((x =house.copy$sqft_lot)-predict.train.sqft_lot)**2)
MSE.test.sqft_lot <- mean(((x =house.copy$sqft_lot)-predict.test.sqft_lot)**2)
MSE.train.sqft_lot
MSE.test.sqft_lot
sqrt(MSE.train.sqft_lot)
sqrt(MSE.test.sqft_lot)
mean(house.copy$sqft_lot)
## ���� ����� �� ����� ����� ��� ����� ���� ���� ������� ������ �� ����� ����


